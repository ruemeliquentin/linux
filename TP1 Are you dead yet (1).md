# TP1 : Are you dead yet ?

---

**🌞 Voici mes multiples façons d'exploser un linux :**

➜ Première Technique : 

Supprimer le dossier racine :

```  
quentin@quentin-VirtualBox:~$ rm -rf / --no-preserve-root
```

➜ Deuxième Technique : 

Rendre les périphériques (souris/clavier) inutilisable : 

```
quentin@quentin-VirtualBox:~$ xinput --list
quentin@quentin-VirtualBox:~$ xinput disable 11
quentin@quentin-VirtualBox:~$ xinput disable 12
```

➜ Troisième Technique : 

Retirer tous les droits au dossier systemd :

```
quentin@quentin-VirtualBox:~$ chmod 000 /lib/systemd/systemd
```

➜ Quatrième Technique : 

Saturer la ram de la machine : 

Il faut tout d'abbord aller dans : 
        - Session and Startup
        - Application Autostart
        - Add
        - command : exo-open --launch TerminalEmulator

Ensuite dans le terminal : 

```
quentin@quentin-VirtualBox:~$ sudo nano ~/.bashrc
```

Dans ce dossier écrire : ```:(){ :|:& };: ```

➜ Cinquième Technique : 

Supprimer l'init (premier process lancé par linux) dans le dossier sbin. Puis de créer un nouveau lien pour que l'init renvoie vers echo et pas vers systemd.

```
quentin@quentin-VirtualBox:~$ cd /sbin
quentin@quentin-VirtualBox:~$ sudo rm init
quentin@quentin-VirtualBox:~$ sudo ln -s /usr/bin/echo init
```

➜ Sixième Technique : 

C'est la technique la plus drastique mais il suffit de se munir d'une batte de Baseball et D'EXPLOSER SON ORDINATEUR, apres ça il est certain que la machine ne marchera plus, rien d'autres d'ailleurs....







