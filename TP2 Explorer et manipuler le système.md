# **TP2 : Explorer et manipuler le système**

## **Prérequis**

### -> Changer le nom de la machine : 

--------------------------
- **Première étape :** changer le nom tout de suite, **jusqu'à ce qu'on redémarre la machine**

    Nous allons tapper cette commande : ```sudo hostname <NOM_MACHINE>```
    ```bash 
    quentin@quentin-VirtualBox:~$ sudo hostname node1.tp2.linux
    ```
    Elle permet de changer **le nom de notre machine** en "node1" jusqu'au **prochain reboot** de celle-ci.
    Nous pouvons vérifier si le changement a été effectué, nous pouvons tapper ceci : ```hostname```
    ```bash
    quentin@quentin-VirtualBox:~$ hostname
    node1.tp2.linux
    ```

Nous voyons ici que le changement de nom **a été pris en compte**.

- **Deuxième étape :** changer le nom qui est pris par la machine quand elle s'allume

    Nous allons maintenant modifier le dossier /home/hostname afin de changer le nom de la machine meme après un reboot.
    Pour ce faire, nous utilisons la commande : ```sudo nano /etc/hostname```
    ```bash
    quentin@quentin-VirtualBox:~$ sudo nano /etc/hostname
    ```
    Une fois dans le fichier texte, nous changeons le nom de la machine par "node1.tp2.linux".
    Comme précédemment, nous allons vérifier si le changement a été compris par la machine : 
    ```bash
    quentin@quentin-VirtualBox:~$ cat /etc/hostname
    node1.tp2.linux
    ```
---

### -> Config réseau fonctionnelle

Nous allons maintenant vérifier que notre VM est **connectée à internet.**
    
Pour ce faire, nous allons tester **plusieurs méthodes** :
    
- **La 1ère :** une connexion à l'adresse ip d'un serveur dns de google (8.8.8.8)
        
```bash
    quentin@quentin-VirtualBox:~$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=24.9 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=25.4 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=20.9 ms
    64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=21.1 ms  
```
Nous voyons que la connexion est réussie avec ce serveur.
    
- **La 2nde :** une connexion au site ynov.com
```bash 
    quentin@quentin-VirtualBox:~$ ping ynov.com
    PING ynov.com (92.243.16.143) 56(84) bytes of data.
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=53 time=32.4 ms
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=53 time=21.7 ms
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=53 time=26.3 ms
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=4 ttl=53 time=37.3 ms
```
Encore une fois, la connexion avec le domaine est réussie.
    
- **La dernière :** une connexion à la VM depuis mon pc
```bash
    quentin@quentin-VirtualBox:~$ ping 192.168.214.17
    PING 192.168.214.17 (192.168.214.17) 56(84) bytes of data.
    64 bytes from 192.168.214.17: icmp_seq=1 ttl=64 time=0.029 ms
    64 bytes from 192.168.214.17: icmp_seq=2 ttl=64 time=0.041 ms
    64 bytes from 192.168.214.17: icmp_seq=3 ttl=64 time=0.042 ms
    64 bytes from 192.168.214.17: icmp_seq=4 ttl=64 time=0.041 ms
```
---

# Partie 1 : SSH

# I. Intro

> *SSH* c'est pour *Secure SHell*.

***SSH* est un outil qui permet d'accéder au terminal d'une machine, à distance, en connaissant l'adresse IP de cette machine.**

*SSH* repose un principe de *client/serveur* :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 22/TCP par convention
- le *client*
  - connaît l'IP du serveur
  - se connecte au serveur à l'aide d'un programme appelé "*client* *SSH*"

Pour nous, ça va être l'outil parfait pour contrôler toutes nos machines virtuelles.

Dans la vie réelle, *SSH* est systématiquement utilisé pour contrôler des machines à distance. C'est vraiment un outil de routine pour tout informaticien, qu'on utilise au quotidien sans y réfléchir.

> *Si vous louez un serveur en ligne, on vous donnera un accès SSH pour le manipuler la plupart du temps.*

# II. Setup du serveur SSH

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

Sur les OS GNU/Linux, les installations se font à l'aide d'un gestionnaire de paquets.

🌞 **Installer le paquet `openssh-server`**

- avec une commande `apt install`

```bash
quentin@quentin-VirtualBox:~$ sudo -i
[sudo] password for quentin:
root@node1:~# apt install openssh-server
Reading package lists... Done
Building dependency tree
Reading state information... Done
openssh-server is already the newest version (1:8.2p1-4ubuntu0.3).
0 upgraded, 0 newly installed, 0 to remove and 89 not upgraded.
```
---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `ssh`
- un dossier de configuration `/etc/ssh/`

## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

- avec une commande `systemctl start`
- vérifier que le service est actuellement actif avec une commande `systemctl status`

```bash=
root@node1:~# systemctl start ssh
root@node1:~# systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2021-11-17 10:04:49 CET; 1h 2min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 534 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 557 (sshd)
      Tasks: 1 (limit: 1105)
     Memory: 5.6M
     CGroup: /system.slice/ssh.service
             └─557 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

nov. 17 10:04:49 quentin-VirtualBox systemd[1]: Starting OpenBSD Secure Shell server...
nov. 17 10:04:49 quentin-VirtualBox sshd[557]: Server listening on 0.0.0.0 port 22.
nov. 17 10:04:49 quentin-VirtualBox sshd[557]: Server listening on :: port 22.
nov. 17 10:04:49 quentin-VirtualBox systemd[1]: Started OpenBSD Secure Shell server.
nov. 17 10:11:41 quentin-VirtualBox sshd[1436]: Accepted password for quentin from 192.168.214.1 port 5>nov. 17 10:11:41 quentin-VirtualBox sshd[1436]: pam_unix(sshd:session): session opened for user quentin>lines 1-18/18 (END)
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2021-11-17 10:04:49 CET; 1h 2min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 534 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 557 (sshd)
      Tasks: 1 (limit: 1105)
     Memory: 5.6M
     CGroup: /system.slice/ssh.service
             └─557 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

nov. 17 10:04:49 quentin-VirtualBox systemd[1]: Starting OpenBSD Secure Shell server...
nov. 17 10:04:49 quentin-VirtualBox sshd[557]: Server listening on 0.0.0.0 port 22.
nov. 17 10:04:49 quentin-VirtualBox sshd[557]: Server listening on :: port 22.
nov. 17 10:04:49 quentin-VirtualBox systemd[1]: Started OpenBSD Secure Shell server.
nov. 17 10:11:41 quentin-VirtualBox sshd[1436]: Accepted password for quentin from 192.168.214.1 port 54086 ssh2
nov. 17 10:11:41 quentin-VirtualBox sshd[1436]: pam_unix(sshd:session): session opened for user quentin by (uid=0)
```

> Vous pouvez aussi faire en sorte que le *service* SSH se lance automatiquement au démarrage avec la commande `systemctl enable ssh`.

## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  - avec une commande `systemctl status`
- afficher le/les processus liés au *service* `ssh`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
  ```bash
root@node1:/# ps aux | grep ssh

root        1436  0.0  0.9  14016  9060 ?        Ss   10:25   0:00 sshd: quentin [priv]

quentin     1516  0.0  0.5  14016  5916 ?        S    10:25   0:00 sshd: quentin@pts/1
```
- afficher le port utilisé par le *service* `ssh`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
  ```bash
  root@node1:/# ss -l
tcp   LISTEN 0      128                                          [::]:ssh                            [::]:*
```
- afficher les logs du *service* `ssh`
  - avec une commande `journalctl`
  - en consultant un dossier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs
  ```bash
  root@node1:/# journalctl | grep sshd
  oct. 19 16:41:24 quentin-VirtualBox useradd[1491]: new user: name=sshd, UI123, GID=65534, home=/run/sshd, shell=/usr/sbin/nologin, from=none
  oct. 19 16:41:24 quentin-VirtualBox usermod[1499]: change user 'sshd' password
  oct. 19 16:41:24 quentin-VirtualBox chage[1506]: changed password expiry for sshd
  oct. 19 16:41:26 quentin-VirtualBox sshd[1622]: Server listening on 0.0.0.0 port 22.
  oct. 19 16:41:26 quentin-VirtualBox sshd[1622]: Server listening on :: port 22.
  oct. 19 16:42:19 quentin-VirtualBox sudo[2733]:  quentin : TTY=pts/0 ; PWD=/home/quentin ; USER=root ; COMMAND=/usr/bin/systemctl enable --now sshd
```

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un **client SSH**

```bash

C:\Users\rueme>ssh quentin@192.168.214.17
quentin@192.168.214.17's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

89 updates can be applied immediately.
38 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Wed Nov 17 10:11:42 2021 from 192.168.214.1
```
## 4. Modification de la configuration du serveur

Pour modifier comment un *service* se comporte il faut modifier le fichier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/ssh/sshd_config`
  - c'est un simple fichier texte
  - modifiez-le comme vous voulez, je vous conseille d'utiliser `nano` en ligne de commande
- effectuez le modifications suivante :
  - changer le ***port d'écoute*** du service *SSH*
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
  ```bash
  root@node1:~# nano /etc/ssh/sshd_config
  root@node1:~# cat /etc/ssh/sshd_config
      Port 1026
    ```
- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

```bash
    root@node1:~# systemctl restart sshd
    root@node1:~# ss -ltpn |grep sshd
    LISTEN    0         128                0.0.0.0:1026             0.0.0.0:*        users:(("ssh",pid=27108,fd=3))
    LISTEN    0         128                   [::]:1026                [::]:*        users:(("ssh",pid=27108,fd=4))
```

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client SSH*

```bash
root@node1:~# ssh -p 1026 quentin@192.168.214.17
The authenticity of host '[192.168.214.17]:1026 ([192.168.214.17]:1026)' can't be established.
ECDSA key fingerprint is SHA256:/ZaQLNbu/2lSm8zzxLWJiDFWuUS8RYiJHigZbEXfEpE.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[192.168.214.17]:1026' (ECDSA) to the list of known hosts.
quentin@192.168.214.17's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

89 updates can be applied immediately.
38 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Wed Nov 17 18:18:17 2021 from 192.168.214.1
```

# Partie 2 : FTP

# I. Intro

> *FTP* c'est pour *File Transfer Protocol*.

***FTP* est un protocole qui permet d'envoyer simplement des fichiers sur un serveur à travers le réseau.**

*FTP* repose un principe de client/serveur :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 21/TCP par convention
- le *client*
  - connaît l'IP du *serveur*
  - se connecte au *serveur* à l'aide d'un programme appelé "*client FTP*"

Dans la vie réelle, *FTP* est souvent utilisé pour échanger des fichiers avec un serveur de façon sécurisée. En vrai ça commence à devenir oldschool *FTP*, mais c'est un truc très basique et toujours très utilisé.

> Si vous louez un serveur en ligne, on vous donnera parfois un accès *FTP* pour y déposer des fichiers.


# II. Setup du serveur FTP

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `vsftpd`
- un fichier de configuration `/etc/vsftpd.conf`

> Le paquet s'appelle `vsftpd` pour *Very Secure FTP Daemon*. A l'apogée de l'utilisation de FTP, il n'était pas réputé pour être un protocole très sécurisé. Aujourd'hui, avec des outils comme `vsftpd`, c'est bien mieux qu'à l'époque.

```bash=
root@node1:~# apt install vsftpd
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following NEW packages will be installed:
  vsftpd
0 upgraded, 1 newly installed, 0 to remove and 61 not upgraded.
Need to get 115 kB of archives.
After this operation, 338 kB of additional disk space will be used.
Get:1 http://fr.archive.ubuntu.com/ubuntu focal/main amd64 vsftpd amd64 3.0.3-12 [115 kB]
Fetched 115 kB in 0s (465 kB/s)
Preconfiguring packages ...
Selecting previously unselected package vsftpd.
(Reading database ... 199604 files and directories currently installed.)
Preparing to unpack .../vsftpd_3.0.3-12_amd64.deb ...
Unpacking vsftpd (3.0.3-12) ...
Setting up vsftpd (3.0.3-12) ...
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for systemd (245.4-4ubuntu3.11) ...
```

## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

- avec une commande `systemctl start`
- vérifier que le service est actuellement actif avec une commande `systemctl status`

> Vous pouvez aussi faire en sorte que le service FTP se lance automatiquement au démarrage avec la commande `systemctl enable vsftpd`.
```bash
root@node1:~# systemctl start vsftpd
root@node1:~# systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2021-11-17 18:33:52 CET; 2min 48s ago
   Main PID: 1653 (vsftpd)
      Tasks: 1 (limit: 1105)
     Memory: 520.0K
     CGroup: /system.slice/vsftpd.service
             └─1653 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 17 18:33:52 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 17 18:33:52 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```
🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
  - avec une commande `systemctl status`
- afficher le/les processus liés au service `vsftpd`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
  ```bash
    root@node1:~# ps aux | grep vsftpd
    root        1653  0.0  0.3   6816  3020 ?        Ss   18:33   0:00 /usr/sbin/vsftpd /etc/vsftpd.conf
  ```
- afficher le port utilisé par le service `vsftpd`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
  ```bash
  root@node1:~# ss -ltpn | grep vsftpd
  LISTEN    0         32                       *:21                     *:*        users:(("vsftpd",pid=1653,fd=3))
```
- afficher les logs du service `vsftpd`
  - avec une commande `journalctl`
  - en consultant un fichier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

```bash
  root@node1:~# journalctl | grep vsftpd
  nov. 17 18:33:52 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
  nov. 17 18:33:52 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```





















