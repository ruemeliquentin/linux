# II. Checklist

**🌞 Choisissez et définissez une IP à la VM**
* contenu de votre fichier de conf
```
NAME=enp0s8         
DEVICE=enp0s8        
BOOTPROTO=static    
ONBOOT=yes          
IPADDR=10.250.1.56
NETMASK=255.255.255.0
```
* Et le résultat d'un "ip a" pour me prouver que les changements on pris effet

```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:82:ec:75 brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.56/24 brd 10.250.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe82:ec75/64 scope link
       valid_lft forever preferred_lft forever
```

➜ Connexion SSH fonctionnelle
```
 ssh quentin@10.250.1.56
quentin@10.250.1.56's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Nov 29 23:54:09 2021
```
**🌞 Vous me prouverez que :**
* Le service ssh est actif sur la VM
```
 systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-11-29 23:53:56 CET; 13min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 853 (sshd)
    Tasks: 1 (limit: 4944)
   Memory: 4.2M
   CGroup: /system.slice/sshd.service
           └─853 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256->

Nov 29 23:53:56 localhost.localdomain systemd[1]: Starting OpenSSH server daemon...
Nov 29 23:53:56 localhost.localdomain sshd[853]: Server listening on 0.0.0.0 port 22.
Nov 29 23:53:56 localhost.localdomain sshd[853]: Server listening on :: port 22.
Nov 29 23:53:56 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
Nov 30 00:03:56 localhost.localdomain sshd[1603]: Accepted password for quentin from 10.250.1.1 port 65241 ssh2
Nov 30 00:03:56 localhost.localdomain sshd[1603]: pam_unix(sshd:session): session opened for user quentin by (uid=0)
```

**🌞 Prouvez que vous avez un accès internet**

* avec une commande ping
```
 ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=56 time=20.10 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=56 time=16.9 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=56 time=17.2 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=56 time=17.2 ms
```

**🌞 Prouvez que vous avez de la résolution de nom**
* Un petit ping vers un nom de domaine
```
 ping dofus.com
PING dofus.com (104.18.17.160) 56(84) bytes of data.
64 bytes from 104.18.17.160 (104.18.17.160): icmp_seq=1 ttl=56 time=17.7 ms
64 bytes from 104.18.17.160 (104.18.17.160): icmp_seq=2 ttl=56 time=17.0 ms
64 bytes from 104.18.17.160 (104.18.17.160): icmp_seq=3 ttl=56 time=18.1 ms
64 bytes from 104.18.17.160 (104.18.17.160): icmp_seq=4 ttl=56 time=17.9 ms
```
**🌞 Définissez node1.tp4.linux comme nom à la machine**

* Contenu du fichier /etc/hostname
```
sudo nano /etc/hostname
node1.tp4.linux
```

* Commande hostname
```
hostname
node1.tp4.linux
```

# III. Mettre en place un service

## 2. Install

**🌞 Installez NGINX**
```
 sudo dnf install nginx
Last metadata expiration check: 0:00:45 ago on Tue 30 Nov 2021 12:16:19 AM CET.
Package nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

## 3. Analyse

**🌞 Analysez le service NGINX**

* Avec une commande ps
```
sudo ps -ef | grep nginx
root       25804       1  0 00:31 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      25805   25804  0 00:31 ?        00:00:00 nginx: worker process
quentin    25813    1500  0 00:32 pts/0    00:00:00 grep --color=auto nginx
```
* Avec une commande ss
```
 sudo ss -ltpn | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=25805,fd=8),("nginx",pid=25804,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=25805,fd=9),("nginx",pid=25804,fd=9))
```

* En regardant la conf
```
root         /usr/share/nginx/html;
```

* Inspectez les fichiers de la racine web
```
ls -la /usr/share/nginx/html/
total 20
drwxr-xr-x. 2 root root   99 Nov 30 00:16 .
drwxr-xr-x. 4 root root   33 Nov 30 00:16 ..
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
```

## 4. Visite du service web

**🌞 Configurez le firewall**
```
 sudo firewall-cmd --add-port=80/tcp --permanent
success
```
```
 sudo firewall-cmd --reload
success
```
**🌞 Tester le bon fonctionnement du service**

* Requêtes HTTP depuis le terminal
```
curl http://10.250.1.56
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
```

## 5. Modif de la conf du serveur web
**🌞 Changer le port d'écoute**
```
 cat /etc/nginx/nginx.conf | grep listen
        listen       8080 default_server;
        listen       [::]:8080 default_server;
```

```
 sudo systemctl restart nginx
[quentin@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-11-30 00:46:47 CET; 17s ago
  Process: 25914 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 25911 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 25909 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 25916 (nginx)
    Tasks: 2 (limit: 4944)
   Memory: 3.6M
   CGroup: /system.slice/nginx.service
           ├─25916 nginx: master process /usr/sbin/nginx
           └─25917 nginx: worker process

Nov 30 00:46:47 node1.tp4.linux systemd[1]: nginx.service: Succeeded.
Nov 30 00:46:47 node1.tp4.linux systemd[1]: Stopped The nginx HTTP and reverse proxy server.
Nov 30 00:46:47 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 30 00:46:47 node1.tp4.linux nginx[25911]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 30 00:46:47 node1.tp4.linux nginx[25911]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 30 00:46:47 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid arg>
Nov 30 00:46:47 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```

```
 sudo firewall-cmd --remove-port=80/tcp --permanent
success
```

```
 sudo firewall-cmd --add-port=8080/tcp --permanent
success
```

```
 sudo firewall-cmd --reload
success
```

```
 sudo ss -ltpn | grep nginx
LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=25917,fd=8),("nginx",pid=25916,fd=8))
LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=25917,fd=9),("nginx",pid=25916,fd=9))
```

```
 curl http://10.250.1.56:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
```

**🌞 Changer l'utilisateur qui lance le service**
```
[quentin@node1 ~]$ sudo useradd web -m -s /bin/sh -u 2000
```

``` sudo passwd web
Changing password for user web.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
Sorry, passwords do not match.
New password:
BAD PASSWORD: The password fails the dictionary check - it does not contain enough DIFFERENT characters
Retype new password:
Sorry, passwords do not match.
New password:
BAD PASSWORD: The password fails the dictionary check - it is based on a dictionary word
Retype new password:
passwd: all authentication tokens updated successfully.
```
(Grosse galère pour un mot de passe...)

**🌞 Changer l'emplacement de la racine Web**
```
sudo mkdir /var/www
[quentin@node1 ~]$ cd /var/www
[quentin@node1 www]$ sudo chmod -R 777 www/
chmod: cannot access 'www/': No such file or directory
[quentin@node1 www]$ sudo chmod -R 777 www
chmod: cannot access 'www': No such file or directory
[quentin@node1 www]$ sudo chmod -R 777 /www
chmod: cannot access '/www': No such file or directory
[quentin@node1 www]$ cd ..
[quentin@node1 var]$ sudo chmod -R 777 www/
[quentin@node1 var]$ sudo su - web
[web@node1 ~]$ cd /var/www
[web@node1 www]$ mkdir super_site_web
```



























































