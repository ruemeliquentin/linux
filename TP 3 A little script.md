# TP 3 : A little script

-[ Script carte d'identité](#script-carte-didentité)

-[ Script youtube-dl](#script-youtube-dl)

-[ MAKE IT A SERVICE](#make-it-a-service)

## Script carte d'identité
📁 **Fichier[`/srv/idcard/idcard.sh`](idcard.sh)**
```
quentin@quentin-VirtualBox:~$ sudo bash /srv/idcard/idcard.sh
Machine name : quentin-VirtualBox
OS Ubuntu and kernel version is 5.11.0-38-generic
IP : 192.168.214.19
RAM : 394Mebi / 978Mebi
Disque : 2,4Go Left
Top 5 processes by RAM usage :
- 958 xfwm4
- 564 /usr/lib/xorg/Xorg
- 1045 /usr/bin/python3
- 987 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0
- 994 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0
Listening ports :
- 53 : systemd-resolve
- 22 : sshd
- 631 : cupsd
- 22 : sshd
- 631 : cupsd

/srv/idcard/idcard.sh: line 23: curl: command not found
Here's your random cat :
```

## Script youtube-dl
📁 **Le script[`/srv/yt/yt.sh`](yt.sh)**

📁 **Le fichier de log[`/var/log/yt/download.log`](download.log)**
```
sudo bash /srv/yt/yt.sh https://www.youtube.com/watch?v=sNx57atloH8
    Video https://www.youtube.com/watch?v=sNx57atloH8
    File path : /srv/yt/downloads/tomato anxiety/tomato anxiety.mp4
```

## MAKE IT A SERVICE
📁 **Le script[`/srv/yt/yt-v2.sh`](yt-v2.sh)**

📁 **Fichier[`/etc/systemd/system/yt.service`](yt.service)**
```
systemctl status yt.service
    ● yt.service - "Processus pour dl des vidéos"
        Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: enabled)
        Active: active (running) since Fri 2021-11-19 12:31:23 CET; 1min 45s ago
    Main PID: 4800 (bash)
        Tasks: 2 (limit: 2312)
        Memory: 772.0K
        CGroup: /system.slice/yt.service
                ├─4800 /usr/bin/bash /srv/yt/yt-v2.sh
                └─4887 sleep 5s

    nov. 19 12:31:23 node1.tp2.linux systemd[1]: Started "Processus pour dl des vidéos".
    nov. 19 12:31:39 node1.tp2.linux bash[4802]: Video https://www.youtube.com/watch?v=sNx57atloH8 was downloaded
    nov. 19 12:31:39 node1.tp2.linux bash[4802]: File path : /srv/yt/downloads/tomato anxiety/tomato anxiety.mp4
```

```
journalctl -xe -u yt
    [...]
    -- The job identifier is 3507.
    nov. 19 12:31:39 node1.tp2.linux bash[4802]: Video https://www.youtube.com/watch?v=sNx57atloH8 was downloaded
    nov. 19 12:31:39 node1.tp2.linux bash[4802]: File path : /srv/yt/downloads/tomato anxiety/tomato anxiety.mp4
```
